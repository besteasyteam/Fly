﻿using Fly.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fly.Domain.Infrastructure;

namespace Fly.Domain.Services
{
    public class PostCategoryService : BaseService<PostCategory, int>
    {
        /// <summary>
        /// 读取帖子分类列表
        /// </summary>
        /// <returns></returns>
        public GetListsResponse<PostCategory> GetPostCategories()
        {
            var response = new GetListsResponse<PostCategory>();
            var result = DbBase.Query<PostCategory>().ToList();
            if(result!=null && result.Count > 0)
            {
                response.IsSuccess = true;
                response.Message = "获取成功";
                response.Items = result;
                return response;
            }
            response.Message = "暂无数据";
            return response;
        }
    }
}
