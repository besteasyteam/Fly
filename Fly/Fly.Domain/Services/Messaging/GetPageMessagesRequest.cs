﻿using Fly.Domain.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fly.Domain.Services.Messaging
{
    public class GetPageMessagesRequest : RequestBaseOfPaging
    {
        public GetPageMessagesRequest(int pageIndex, int pageSize,int userId) : base(pageIndex, pageSize)
        {
            UserId = userId;
        }
        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; private set; }
    }
}
