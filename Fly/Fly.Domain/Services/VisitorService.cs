﻿using Fly.Domain.Infrastructure;
using Fly.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fly.Domain.Services
{
    /// <summary>
    /// 访客服务类
    /// </summary>
    public class VisitorService : BaseService<Visitor, int>
    {
        /// <summary>
        /// 获取最新到访的12人
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public GetListsResponse<Visitor> GetTopTwelve(int userId)
        {
            var response = new GetListsResponse<Visitor>();

            var result = DbBase.Query<Visitor>()
                .Include(p => p.VisitorUser)
                .Where(p => p.UserId == userId)
                .OrderByDescending(p => p.LastTime)
                .ToPage(1, 12);
            if (result.Items != null && result.Items.Count > 0)
            {
                response.IsSuccess = true;
                response.Message = "获取成功";
                response.Items = result.Items;
                return response;
            }

            response.Message = "暂无访客";

            return response;
        }
        /// <summary>
        /// 更新访客信息
        /// </summary>
        /// <param name="entity"></param>
        public void AddVisitor(Visitor entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            ThrowExceptionIfEntityIsInvalid(entity);

            var con = DbBase.Query<Visitor>().SingleOrDefault(p => p.VisitorUserId == entity.VisitorUserId && p.UserId == entity.UserId);
            if (con != null)
            {
                con.LastTime = DateTime.Now;
                DbBase.UpdateMany<Visitor>().OnlyFields(p => p.LastTime).Where(p => p.Id == con.Id).Execute(con);
                return;
            }
            DbBase.Insert(entity);
        }


    }
}
