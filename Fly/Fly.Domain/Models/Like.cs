﻿using Fly.Domain.Infrastructure;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fly.Domain.Models
{
    [TableName("Likes")]
    [PrimaryKey("Id")]
    public class Like : BaseEntity<int>
    {
        /// <summary>
        /// 评论ID
        /// </summary>
        public int CommentId { get; set; }
        /// <summary>
        /// 用户id
        /// </summary>
        public int UserId { get; set; }
        protected override void Validate()
        {
            //throw new NotImplementedException();
        }
    }
}
