﻿using Fly.Domain.Infrastructure;
using Fly.Domain.Services;
using NPoco;
using System;

using Fly.Domain.Extension;

namespace Fly.Domain.Models
{
    /// <summary>
    /// 用户实体
    /// </summary>
    [TableName("Users")]
    [PrimaryKey("Id")]
    public class User : BaseEntity<int>
    {
        /// <summary>
        /// 电子邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 邮箱是否已验证
        /// </summary>
        public bool EmailConfirmed { get; set; }
        /// <summary>
        /// 邮箱验证Token
        /// </summary>
        public string EmailConfirmToken { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; set; }
        /// <summary>
        /// 称号
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 个性签名
        /// </summary>
        public string Sign { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string HeadPortrait { get; set; }
        /// <summary>
        /// 积分
        /// </summary>
        public int Integral { get; set; }
        /// <summary>
        /// 是否VIP
        /// </summary>
        public bool IsVip { get; set; }
        /// <summary>
        /// VIP等级
        /// </summary>
        public int VipLevel { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 是否已禁用
        /// </summary>
        public bool IsDisabled { get; set; }
        /// <summary>
        /// 邮箱是否可修改
        /// </summary>
        public bool EmailIsUpdate { get; set; }
        /// <summary>
        /// 是否管理员
        /// </summary>
        public bool IsAdmin { get; set; }


        /// <summary>
        /// 该用户发表的帖子总数
        /// </summary>
        [Ignore]
        public int PostCount
        {
            get
            {
                return new PostService().GetPostCountByUserId(Id);
            }
        }

        protected override void Validate()
        {
            var _service = new UserService();

            if (string.IsNullOrEmpty(Email))
                AddBrokenRule(new BusinessRule("Email", "邮箱地址不能为空."));
            if (!Email.IsEmail())
                AddBrokenRule(new BusinessRule("Email", "邮箱地址不正确."));
            if (Id == 0)
            {
                if (_service.CheckEmailIsExists(Email))
                    AddBrokenRule(new BusinessRule("Email", "邮箱地址已存在."));
            }
            if (string.IsNullOrEmpty(Nickname))
                AddBrokenRule(new BusinessRule("Email", "昵称不能为空."));
            if (Id == 0)
            {
                if (_service.CheckNicknameIsExists(Nickname))
                    AddBrokenRule(new BusinessRule("Nickname", "昵称已存在."));
            }

            if (string.IsNullOrEmpty(Password))
                AddBrokenRule(new BusinessRule("Password", "密码不能为空."));
        }
    }
}
