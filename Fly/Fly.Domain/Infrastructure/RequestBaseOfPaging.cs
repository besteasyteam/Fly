﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fly.Domain.Infrastructure
{
    public class RequestBaseOfPaging
    {
        public RequestBaseOfPaging(int pageIndex,int pageSize)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
        }
        public int PageIndex { get; private set; }
        public int PageSize { get; private set; }
    }
}
