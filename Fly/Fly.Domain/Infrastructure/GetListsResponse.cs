﻿using Fly.Domain.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fly.Domain.Infrastructure
{
    public class GetListsResponse<T> : ResponseBase
    {
        public IList<T> Items { get; set; }
    }
}
