﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace Fly.Domain.Authentication
{

    public class AspFormsAuthentication : IFormsAuthentication
    {
        public void SetAuthenticationToken(string token, bool createPersistentCookie = false)
        {
            FormsAuthentication.SetAuthCookie(token, createPersistentCookie);
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
}
