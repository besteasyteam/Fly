﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Fly.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //错误页面
            //404
            routes.MapRoute("Error404", "error/404", new { controller = "Error", action = "NotFound" });
            //500
            routes.MapRoute("Error500", "error/500", new { controller = "Error", action = "Error500" });

            //上传图片
            routes.MapRoute("Upload", "user/upload/", new { controller = "Account", action = "Upload" });
            //注销登录
            routes.MapRoute("LogOut", "user/logout/", new { controller = "Account", action = "LogOut" });
            //登录
            routes.MapRoute("Login", "user/login/", new { controller = "Account", action = "Login" });
            //注册
            routes.MapRoute("Register", "user/reg/", new { controller = "Account", action = "Register" });
            //用户中心
            routes.MapRoute("UserIndex", "user/", new { controller = "Account", action = "Index" });
            //忘记密码
            routes.MapRoute("Forget", "user/forget/", new { controller = "Account", action = "Forget" });
            //帐号设置
            routes.MapRoute("Set", "user/set/", new { controller = "Account", action = "Set" });
            //激活邮箱
            routes.MapRoute("Activate", "user/activate/", new { controller = "Account", action = "Activate" });
            //我的消息
            routes.MapRoute("Message", "user/msg", new { controller = "Account", action = "Message" });
            //修改密码
            routes.MapRoute("Repass", "user/repass", new { controller = "Account", action = "Repass" });
            //用户主页
            routes.MapRoute("UserHome", "user/{userId}", new { controller = "Account", action = "Home" });
            //激活邮箱
            routes.MapRoute("EmailValidate", "emailvalidate/{token}", new { controller = "Account", action = "EmailValidateResult" });
            //@搜索
            routes.MapRoute("SearchJump", "jump/{nickname}", new { controller = "Account", action = "Jump" });

            //消息相关 API
            //用户主页
            //获取个人消息
            routes.MapRoute("MessageGet", "api/msg/", new { controller = "Message", action = "Get" });
            //获取未读消息总数 /api/msg-count
            routes.MapRoute("MessageCount", "api/msg-count/", new { controller = "Message", action = "MsgCount" });
            //将消息设置为已读 /api/msg-read
            routes.MapRoute("MessageRead", "api/msg-read/", new { controller = "Message", action = "MsgRead" });
            //我的求解 /api/mine-post
            routes.MapRoute("MessageMinePost", "api/mine-post/", new { controller = "Post", action = "MinePost" });
            //删除一条消息 /api/msg-del/
            routes.MapRoute("MessageDel", "api/msg-del/", new { controller = "Message", action = "MsgDel" });
            //发送激活邮件 /api/activate/
            routes.MapRoute("SendActivate", "api/activate/", new { controller = "Account", action = "SendActivate" });
            //上传图片 /api/upload/
            routes.MapRoute("PostUpload", "api/upload/", new { controller = "Post", action = "Upload" });
            //删除帖子 /api/jie-delete/
            routes.MapRoute("PostDelete", "api/jie-delete/", new { controller = "Post", action = "Delete" });
            //帖子设置 /api/jie-set/
            routes.MapRoute("PostSet", "api/jie-set/", new { controller = "Post", action = "Set" });
            //赞帖子 /api/jieda-zan/
            routes.MapRoute("PostJieDaZan", "api/jieda-zan/", new { controller = "Post", action = "JieDaZan" });

            //帖子相关
            //帖子采纳
            routes.MapRoute("PostAccept", "post/accept/", new { controller = "Post", action = "Accept" });
            //回帖
            routes.MapRoute("PostReply", "post/reply/", new { controller = "Post", action = "Reply" });
            //未结贴
            routes.MapRoute("PostUnsolved", "post/unsolved/", new { controller = "Post", action = "Unsolved" });
            //未结贴 -- 分页
            routes.MapRoute("PostUnsolvedPage", "post/unsolved_page_{pageIndex}", new { controller = "Post", action = "Unsolved" });
            //已采纳
            routes.MapRoute("PostSolved", "post/solved/", new { controller = "Post", action = "Solved" });
            //已采纳 -- 分页
            routes.MapRoute("PostSolvedPage", "post/solved_page_{pageIndex}", new { controller = "Post", action = "Solved" });
            //帖子主页
            routes.MapRoute("PostHome", "post/", new { controller = "Post", action = "Index" });
            //帖子列表-分页
            routes.MapRoute("PostHomePage", "post/page_{pageIndex}", new { controller = "Post", action = "Index" });
            //发布帖子
            routes.MapRoute("PostAdd", "post/add", new { controller = "Post", action = "Add" });
            //编辑帖子
            routes.MapRoute("PostEdit", "post/edit_{postId}", new { controller = "Post", action = "Edit" });
            //精贴
            routes.MapRoute("PostWonderful", "post/wonderful/", new { controller = "Post", action = "Wonderful" });
            //精贴 - 分页
            routes.MapRoute("PostWonderfulPage", "post/wonderful_page_{pageIndex}", new { controller = "Post", action = "Wonderful" });
            //查看帖子详情
            routes.MapRoute("PostDetail", "post/{postId}", new { controller = "Post", action = "Detail" });


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
