﻿using Fly.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Fly.Domain.Services.Messaging;

namespace Fly.Web.Controllers
{
    public class MessageController : BaseController
    {
        // 获取消息列表
        public ActionResult Get()
        {

            var request = new GetPageMessagesRequest(1, 30, CurrentUserId);
            var response = new MessageService().GetPageMessagesByFilter(request);
            if (response.IsSuccess)
            {
                var rows = response.Pages.Items.Select(p =>
                {
                    return new
                    {
                        id = p.Id,
                        href = Domain + p.Href,
                        content = p.Content,
                        time = p.TimeAgo,
                        nickname = p.FormUser.Nickname,
                        status=p.MessageType.GetHashCode()
                    };
                });

                return JsonResultForFly(0, "获取成功", rows);

            }
            return JsonResultForFly(0, "暂无信息", new { });
        }
        //获取消息总数
        public ActionResult MsgCount()
        {
            return JsonResultForFly(0, "获取成功", new Dictionary<string, object>
            {
                { "count", new MessageService().GetMsgCount(CurrentUserId) }
            });
        }
        //将消息设置为已读
        public ActionResult MsgRead()
        {
            var response = new MessageService().SetRead(CurrentUserId);

            return JsonResultForFly(response.IsSuccess ? 0 : 1, response.Message);
        }
        //删除单条消息
        public ActionResult MsgDel()
        {
            var type = Request["type"];
            if (type != null && type == "all")
            {
                var response = new MessageService().DeleteAllMessages(CurrentUserId);
                return JsonResultForFly(response.IsSuccess ? 0 : 1, response.Message);
            }
            else
            {
                var id = Request["id"];
                var response = new MessageService().DeleteOneMessage(Convert.ToInt32(id), CurrentUserId);
                return JsonResultForFly(response.IsSuccess ? 0 : 1, response.Message);
            }
        }
    }
}