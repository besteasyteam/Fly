/*
Navicat MySQL Data Transfer

Source Server         : 本地数据库
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : fly_beginner

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-03-13 20:59:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) DEFAULT NULL,
  `PostId` int(11) DEFAULT NULL,
  `Like` int(11) DEFAULT '0',
  `CreateTime` datetime DEFAULT NULL,
  `IsAdopt` bit(1) DEFAULT b'0',
  `Content` text,
  `Ticks` bigint(20) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for contributions
-- ----------------------------
DROP TABLE IF EXISTS `contributions`;
CREATE TABLE `contributions` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) DEFAULT NULL,
  `Time` varchar(50) DEFAULT NULL,
  `Number` int(11) DEFAULT NULL,
  `UpdateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for likes
-- ----------------------------
DROP TABLE IF EXISTS `likes`;
CREATE TABLE `likes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CommentId` int(11) DEFAULT NULL,
  `UserId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for links
-- ----------------------------
DROP TABLE IF EXISTS `links`;
CREATE TABLE `links` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Url` varchar(50) DEFAULT NULL,
  `Sort` int(11) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `FormId` int(11) DEFAULT NULL,
  `ToId` int(11) DEFAULT NULL,
  `Href` varchar(256) DEFAULT NULL,
  `Content` varchar(256) DEFAULT NULL,
  `CreateTime` datetime DEFAULT NULL,
  `MessageType` int(11) DEFAULT NULL,
  `IsRead` bit(1) DEFAULT b'0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for postcategories
-- ----------------------------
DROP TABLE IF EXISTS `postcategories`;
CREATE TABLE `postcategories` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(50) DEFAULT NULL,
  `Content` text,
  `UserId` int(11) DEFAULT NULL,
  `PostCategoryId` int(11) DEFAULT NULL,
  `Reward` int(11) DEFAULT NULL,
  `PostStatus` int(11) DEFAULT NULL,
  `IsBoutique` bit(1) DEFAULT b'0',
  `IsTop` bit(1) DEFAULT b'0',
  `Sort` int(11) DEFAULT '0',
  `CreateTime` datetime DEFAULT NULL,
  `UpdateTime` datetime DEFAULT NULL,
  `Hits` int(11) DEFAULT NULL,
  `Collection` int(11) DEFAULT NULL,
  `IsShow` bit(1) DEFAULT b'0',
  `CommentCount` int(11) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(50) DEFAULT NULL,
  `EmailConfirmed` bit(1) DEFAULT b'0',
  `Password` varchar(50) DEFAULT NULL,
  `Nickname` varchar(50) DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Gender` int(11) DEFAULT NULL,
  `City` varchar(50) DEFAULT NULL,
  `Sign` varchar(126) DEFAULT NULL,
  `HeadPortrait` varchar(126) DEFAULT NULL,
  `Integral` int(11) DEFAULT NULL,
  `IsVip` bit(1) DEFAULT b'0',
  `VipLevel` int(11) DEFAULT NULL,
  `CreateTime` datetime DEFAULT NULL,
  `IsDisabled` bit(1) DEFAULT b'0',
  `EmailIsUpdate` bit(1) DEFAULT b'1',
  `EmailConfirmToken` varchar(50) DEFAULT NULL,
  `IsAdmin` int(11) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for visitors
-- ----------------------------
DROP TABLE IF EXISTS `visitors`;
CREATE TABLE `visitors` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `VisitorUserId` int(11) DEFAULT NULL,
  `UserId` int(11) DEFAULT NULL,
  `LastTime` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
